from django.shortcuts import render, get_object_or_404
from django.db.models import Sum
from .models import Reports

# Create your views here.
def index(request):
    reports = Reports.objects.all()
    reports = reports.filter().values('date').order_by(
        'date').annotate(clicks=Sum('clicks'), revenue=Sum('revenue'))
    return render(request, 'reports/index.html', {'reports': reports})


def publisher(request, report_date):
    reports = Reports.objects.all().filter(date=report_date)
    return render(request, 'reports/publisher.html', {'reports': reports})


def source(request, report_source, report_pub_name, report_date):
    reports = Reports.objects.all().filter(source=report_source, pub_name=report_pub_name, date=report_date).values('source').annotate(
        source_clicks=Sum('clicks'), source_revenue=Sum('revenue'))
    return render(request, 'reports/source.html', {'reports': reports})
