from django.core.management.base import BaseCommand
from reports.models import Reports
from os import path
import csv


class Command(BaseCommand):
    help = "Imports a csv file into your database."

    def add_arguments(self, parser):
        parser.add_argument('filename', type=str,
                            help='Specify filename to import.')

    def handle(self, *args, **kwargs):
        filename = kwargs["filename"]
        filename, extension = path.splitext(filename)

        try:
            if extension == ".csv":
                with open(filename + ".csv") as csvfile:
                    reader = csv.DictReader(csvfile)
                    for row in reader:
                        p, created = Reports.objects.update_or_create(date=row["date"],
                                    pub_name=row["pub_name"],
                                    source=row["source"],
                                    clicks=row["clicks"],
                                    revenue=row["revenue"])
                        p.save()
                self.stdout.write(self.style.SUCCESS(
                    "Successfully imported csv file."))
            else:
                print("File does not appear to be csv.")
        except FileNotFoundError:
            self.stdout.write("File not found.")
