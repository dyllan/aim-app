from django.urls import path
from . import views


app_name = 'reports'

urlpatterns = [
    path('', views.index, name='index'),
    path('date/<report_date>', views.publisher, name='publisher'),
    path('source/<report_source>,<report_pub_name>,<report_date>',
         views.source, name='source')
]
