from django.contrib import admin
from .models import Reports


class ReportsAdmin(admin.ModelAdmin):
    list_display = ("date", "pub_name", "source", "clicks", "revenue")


# Register your models here.
admin.site.register(Reports, ReportsAdmin)
