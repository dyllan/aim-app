from django.db import models
from django.db.models.fields import CharField, DateField, DateTimeField, DecimalField, FloatField, IntegerField

# Create your models here.


class Reports(models.Model):
    date = DateField()
    pub_name = CharField(max_length=50)
    source = CharField(max_length=50)
    clicks = IntegerField()
    revenue = DecimalField(max_digits=19, decimal_places=2)

    class Meta:
        verbose_name = "Report"
        verbose_name_plural = "Reports"

    def __str__(self) -> str:
        return self.pub_name
